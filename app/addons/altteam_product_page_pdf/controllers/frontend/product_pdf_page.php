<?php
/*****************************************************************************
 * This is a commercial software, only users who have purchased a  valid
 * license and accepts the terms of the License Agreement can install and use  
 * this program.
 *----------------------------------------------------------------------------
 * @copyright  LCC Alt-team: http://www.alt-team.com
 * @license    http://www.alt-team.com/addons-license-agreement.html
****************************************************************************/
use Tygh\Tygh;

if (!defined('BOOTSTRAP')) {
    die('Access denied');
}

if($mode == 'pdf'){
    if(!empty($_REQUEST['product_id'])){
        echo(fn_create_product_pdf_page($_REQUEST['product_id'],Tygh::$app['session']['auth']));
    }
    exit;
}