<?php

/*****************************************************************************
 * This is a commercial software, only users who have purchased a  valid
 * license and accepts the terms of the License Agreement can install and use  
 * this program.
 *----------------------------------------------------------------------------
 * @copyright  LCC Alt-team: http://www.alt-team.com
 * @license    http://www.alt-team.com/addons-license-agreement.html
 ****************************************************************************/

use Tygh\Tygh;
use Tygh\Registry;
use Tygh\Pdf;

if (!defined('BOOTSTRAP')) {
    die('Access denied');
}

function fn_create_product_pdf_page($product_id, $auth)
{
    $featuries = [];
    $product_featuries = [];

    $product_data = fn_get_product_data($product_id, $auth);
    $logos = fn_get_logos(Registry::get('runtime.company_id'));
    $featuries =  fn_get_product_features_list($product_data, 'CP');

    if (!empty($featuries)) {
        foreach ($featuries as $_featuries) {
            if ($_featuries['parent_id']  != 0) {
                if (!empty($product_featuries['with_sub'][$_featuries['parent_id']])) {
                    $product_featuries['with_sub'][$_featuries['parent_id']][] = [
                        'group_name' => fn_get_feature_name($_featuries['parent_id']),
                    ];
                }else{
                    $product_featuries['with_sub'][$_featuries['parent_id']][] = [
                        'group_name' => fn_get_feature_name($_featuries['parent_id']),
                    ];
                }
                $product_featuries['with_sub'][$_featuries['parent_id']]['featuries'][] = [
                    'feature_name' => $_featuries['description'],
                    'variants' => $_featuries['variants'],
                ];
            } else {
                $product_featuries['non_sub'][] = [
                    'feature_name' =>  $_featuries['description'],
                    'variants' => $_featuries['variants']
                ];
            }
        }
    }

    $_pdf_data = [
        'product_id' => $product_id,
        'main_image' => [
            'img' => $product_data['main_pair']['detailed']['image_path'],
            'width' => $product_data['main_pair']['detailed']['image_x'],
            'height' => $product_data['main_pair']['detailed']['image_y'],
        ],
        'product_features' => $product_featuries,
        'description' => $product_data['full_description'],
        'logo' => $logos['theme']['image']['image_path']
    ];

    $view = Tygh::$app['view'];
    $view->assign('pdf_creator', $_pdf_data);

    $template = $view->displayMail('addons/altteam_product_page_pdf/pdf_creator.tpl', false, 'A');

    $html[] = $template;

    $filename = "product_page_pdf_$product_id";
    $result = Pdf::render($html, $filename, false);

    return  !empty($filename) ? $filename : $result;
}