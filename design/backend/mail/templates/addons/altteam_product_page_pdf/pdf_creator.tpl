<!DOCTYPE html>
<html dir="ltr">
<head>
{literal}
<style type="text/css" media="screen,print">
body {
    padding: 0;
    margin: 0;
}
a, a:link, a:visited, a:hover, a:active {
    color: #000000;
    text-decoration: underline;
}
a:hover {
    text-decoration: none;
}

#print-wrapp {
	max-width: 800px;
	width: 100%;
	margin: 0px;
	text-align: initial;
}
p{
    font-size: 13px;
    font-family: Arial;
}
h4 {
    font-size: 17px;
    font-family: Arial;
    margin:0;
    font-style:italic;
    font-weight:600;
    margin-left:20px;
}
.hr_print {
    margin-top: 7px;
    margin-bottom: 7px;
    height: 2px;
    color:white;
    background-color:#0099ff;
    border:none;
}
b{
    font-weight: 600;
}
tr{
    margin:0;
    padding:0;
}
td{
    margin:0;
    padding:0;
}

.livak{
    width:90%;
}
.simple{
    width: 70%;
}
.simple tr:nth-child(odd){
	background-color: #f4f4f4;
    width: 70%;
}
.simple td{
    padding-left:5px;
}
.hard td{
    padding-left:5px;
}
.table-p{
    font-size:12px;
}
.livak_2{
    width:90%;
}
.hard tr:nth-child(odd){
    background-color: #f4f4f4;
    width: 70%;
}
</style>
{/literal}
</head>
{include file="common/scripts.tpl"}
<body>

<table id="print-wrapp">
    <tr>
        <td align="center" style="padding-bottom: 3px;"><img src="{$pdf_creator.logo}" height="70px" border="0" /></td>
        <tr>
            <td>
                <hr  class="hr_print" noshade width="100%"/>
            </td>
        </tr>
    </tr>
    <tr>
		<td>
            <img src="{$pdf_creator.main_image.img}" height="150px"  />
        </td>
    </tr>
</table>
<div>
    {if $pdf_creator.description}
        <div>        
                <hr  class="hr_print" noshade width="100%"/>
                <h4>{__("description")}</h4>
                <hr class="hr_print"  noshade width="100%"/>
                <div>
                    {$pdf_creator.description nofilter}
                <div>
        </div>
    {/if}
    {if $pdf_creator.product_features}
        <div>
            <hr  class="hr_print"noshade width="100%"/>
            <h4>{__("features")}</h4>
            <hr class="hr_print" noshade width="100%"/>
        </div>
        <div class="tr">
            <div class="non_sub">
            <table class="livak_2">
                <tbody class="hard">
                    {foreach $pdf_creator.product_features.non_sub as $product_features}
                        <tr>
                            <td>
                                <p class="table-p">{$product_features.feature_name}: &nbsp;&nbsp;&nbsp;{foreach $product_features.variants as $product_feature_variants}{$product_feature_variants.variant}&nbsp;{/foreach}</p>
                            </td>     
                        </tr>
                    {/foreach}
                </tbody>
            </table>
            </div>
            <table class="livak">
                {foreach $pdf_creator.product_features.with_sub as $product_features}
                        <tr>
                            <td><p class="table-p"><b>{$product_features.0.group_name}</b></p></td>
                        </tr>
                        <tbody class="simple">
                            {foreach $product_features.featuries as $sub_featuries}
                                <tr>
                                    <td>
                                        <p class="table-p">{$sub_featuries.feature_name}: &nbsp;&nbsp;&nbsp;{foreach $sub_featuries.variants as $product_feature_variants}{$product_feature_variants.variant}&nbsp;{/foreach}</p>
                                    </td>
                                </tr>     
                            {/foreach}   
                        </tbody>
                {/foreach}
            </table>
            
        </div>
     {/if}
</div>
</body>
</html>